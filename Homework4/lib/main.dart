import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(LogoApp());

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState(){
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addStatusListener((status) {
        if(status == AnimationStatus.completed){
          controller.reverse();
        }
        else if(status == AnimationStatus.dismissed){
          controller.forward();
        }
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: RotationTransition(
          turns: animation,
          child: Image(
            image: AssetImage('images/album.jpg'),
          ),
        )
    );
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }
}
